module cmd/main.go

go 1.16

require (
	github.com/jasonlvhit/gocron v0.0.1
	github.com/spf13/viper v1.8.1
	go.etcd.io/bbolt v1.3.6
)
