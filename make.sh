#bin/sh
#
#
# Find all the go files in the current directory and compile for three platforms
files=`ls -d cmd/*|grep go|grep -vi test`

# OSX
echo "Building for OSX\n"
GOOS=darwin GOARCH=amd64 go build -o builds/osx/dyndns ${files}
GOOS=darwin GOARCH=386 go build -o builds/osx/dyndns-386 ${files}

# Windows
echo "Building for Windows\n"
GOOS=windows GOARCH=386 go build -o builds/win/dyndns.exe ${files}

# Linux
echo "Building for Linux\n"
#CC=x86_64-linux-musl-gcc CXX=x86_64-linux-musl-g++ GOARCH=amd64 GOOS=linux CGO_ENABLED=1 go build -ldflags "-linkmode external -extldflags -static" -o builds/linux/dyndns ${files}
GOOS=linux GOARCH=amd64 go build -o builds/linux/dyndns ${files}
GOOS=linux GOARCH=386 go build -o builds/linux/dyndns-386 ${files}

output=`ls -alFR builds/|grep -v \/|grep -v DS`

echo "${output}"