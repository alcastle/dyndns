package main

import (
	"strings"
	"time"
)

const noipDnsAPI = "dynupdate.no-ip.com/nic/update"

// responseEvalnoip evaluates the HTTP responses from provider DNS API
// Returns the response message from provider
func responseEvalnoip(resp string, host Record) Record {
	// https://www.noip.com/integrate/response

	response := strings.TrimSpace(strings.ReplaceAll(resp, currentIP, ""))
	host.LastCheckDate = time.Now()

	switch response {
	case "good":
		host.Mesg = "DNS hostname update successful."
		host.Wait = 0
		host.Stop = false
		host.CurrentIP = currentIP
		host.LastIPUpdate = time.Now()
	case "nochg":
		host.Mesg = "IP address is current, no update performed."
		host.Wait = 0
		host.Stop = false
		host.CurrentIP = currentIP
	case "nohost":
		host.Mesg = "Hostname supplied does not exist under specified account, client exit and require user to " +
			"enter new login credentials before performing an additional request."
		host.Wait = 0
		host.Stop = true
	case "badauth":
		host.Mesg = "Invalid username password combination."
		host.Wait = 0
		host.Stop = true
	case "badagent":
		host.Mesg = "Client disabled. Client should exit and not perform any more updates without user intervention."
		host.Wait = 0
		host.Stop = true
	case "abuse":
		host.Mesg = "Username is blocked due to abuse. Either for not following our update specifications or " +
			"disabled due to violation of the No-IP terms of service. "
		host.Wait = 0
		host.Stop = true
	case "911":
		host.Mesg = "A fatal error on our side such as a database outage. Retry the update no sooner than 30 minutes."
		host.Wait = 1800
		host.Stop = false
	default:
		host.Mesg = "Unrecognized response: " + response
		host.Wait = 0
		host.Stop = true
	}

	return host
}

// UpdateDNSnoip calls provider API to update the DNS record with the current IP Address.
func UpdateDNSnoip(data hostMap, host Record) Record {

	var u = strings.Join(data.Domain["username"], "")
	var p = strings.Join(data.Domain["password"], "")
	var h = strings.Join(data.Domain["hostname"], "")

	endpoint := "https://" + u + ":" + p + "@" + noipDnsAPI +
		"?hostname=" + h + "&myip=" + strings.TrimSpace(currentIP)

	statusCode, resp := httpRequest(endpoint, "POST")
	host.StatusCode = statusCode
	host = responseEvalnoip(resp, host)

	return host
}
