package main

import (
	"strings"
	"time"
)

const dynuDnsAPI = "api.dynu.com/nic/update"

// responseEvalDynu evaluates the HTTP responses from provider DNS API
// Returns the response message from provider
func responseEvalDynu(resp string, host Record) Record {
	// https://www.dynu.com/DynamicDNS/IP-Update-Protocol#ipupdate

	response := strings.TrimSpace(strings.ReplaceAll(resp, currentIP, ""))
	host.LastCheckDate = time.Now()

	switch response {
	case "good":
		host.CurrentIP = currentIP
		host.Mesg = "The update was successful."
		host.Wait = 0
		host.Stop = false
		host.LastIPUpdate = time.Now()
	case "nochg":
		host.CurrentIP = currentIP
		host.Mesg = "This response code is returned in cases where IP address was found to be unchanged " +
			"on the server side."
		host.Wait = 0
		host.Stop = false
	case "nohost":
		host.Mesg = "This response code is returned in cases where hostname/username is not found in the system."
		host.Wait = 0
		host.Stop = true
	case "badauth":
		host.Mesg = "This response code is returned in case of a failed authentication for the 'request'. " +
			"Please note that sending across an invalid parameter such as an unknown domain name can also " +
			"result in this 'response code'. The client must advise the user to check all parameters including " +
			"authentication parameters to resolve this problem."
		host.Wait = 0
		host.Stop = true
	case "notfqdn":
		host.Mesg = "This response code is returned in cases where the hostname is not a valid fully qualified " +
			"hostname."
		host.Wait = 0
		host.Stop = true
	case "abuse":
		host.Mesg = "This response code is returned in cases where update process has failed due to abusive behaviour."
		host.Wait = 0
		host.Stop = true
	case "911":
		host.Mesg = "This response code is returned in cases where the update is temporarily halted due to " +
			"scheduled maintenance. Client must respond by suspending update process for 10 minutes upon " +
			"receiving this response code."
		host.Wait = 600
		host.Stop = false
	case "dnserr":
		host.Mesg = "This response code is returned in cases where there was an error on the server side. " +
			"The client must respond by retrying the update process."
		host.Wait = 0
		host.Stop = false
	case "!donator":
		host.Mesg = "This response code is returned to indicate that this functionality is only available to members."
		host.Wait = 0
		host.Stop = true
	default:
		if strings.Contains(response, "unknown") {
			host.Mesg = "This response code is returned if an invalid 'request' is made to the API server. " +
				"This 'response code' could be generated as a result of badly formatted parameters as well " +
				"so parameters must be checked for validity by the client before they are passed along with " +
				"the 'request'."
			host.Wait = 0
			host.Stop = true
		} else if strings.Contains(response, "servererror") {
			host.Mesg = "This response code is returned in cases where an error was encountered on the server side. " +
				"The client may send across the request again to have the 'request' processed successfully."
			host.Wait = 0
			host.Stop = false
		} else {
			host.Mesg = "Unrecognized response: " + response
			host.Wait = 0
			host.Stop = true
		}
	}

	return host
}

// UpdateDNSdynu calls provider API to update the DNS record with the current IP Address.
func UpdateDNSdynu(data hostMap, host Record) Record {

	var u = strings.Join(data.Domain["username"], "")
	var p = strings.Join(data.Domain["password"], "")
	var h = strings.Join(data.Domain["hostname"], "")

	endpoint := "https://" + u + ":" + p + "@" + dynuDnsAPI +
		"?hostname=" + h + "&myip=" + strings.TrimSpace(currentIP)

	statusCode, resp := httpRequest(endpoint, "POST")
	host.StatusCode = statusCode
	host = responseEvalDynu(resp, host)

	return host
}
