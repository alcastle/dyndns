package main

import (
	"net"
	"testing"
)

const exampleValidIP = "172.217.14.206"
const exampleURL = "https://google.com"

func Test_GetCurrentIP(t *testing.T) {
	ipAddress := getCurrentIP()

	if ipAddress == "" {
		t.Errorf("Expected valid IP Address, received %v", ipAddress)
	}
}

func Test_IsIP(t *testing.T) {
	validIP := exampleValidIP
	isValid := net.ParseIP(validIP)

	if isValid == nil {
		t.Errorf("isIP failed, expected %v, got %v", isValid, validIP)
	}
}

func Test_UpdateDNS(t *testing.T) {

	//data := hostMap{Domain: map[interface{}][]string{}}
}

func Test_Init(t *testing.T) {
	testing.Init()

	if updateInterval <= 0 {
		t.Errorf("Expected integer for updateInterval, got %v", updateInterval)
	}
}

func Test_Run(t *testing.T) {

}

func Test_GoogleResponseDefault(t *testing.T) {
	// response := "TACOTIME"
}

func Test_GoogleResponseGood(t *testing.T) {
	// response := "good"
}

func Test_GoogleResponseNochg(t *testing.T) {
	// response := "nochg"
}

func Test_GoogleResponseBadAuth(t *testing.T) {
	// response := "badauth"
}

func Test_GoogleResponseBadAgent(t *testing.T) {
	// response := "badagent"
}

func Test_GoogleResponseAbuse(t *testing.T) {
	// response := "abuse"
}

func Test_GoogleResponse911(t *testing.T) {
	// response := "911"
}

func Test_GoogleResponseConflict(t *testing.T) {
	// response := "conflict"
}

func Test_HttpRequest(t *testing.T) {

}

func Test_executeConJob(t *testing.T) {
	// Test not needed
}

func Test_main(t *testing.T) {
	// Test not needed
}
