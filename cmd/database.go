package main

import (
	"encoding/json"
	"fmt"
	bolt "go.etcd.io/bbolt"
	"log"
	"time"
)

const databaseName = "CI-DDNS.db"
const hostBucket = "hosts"
const ipBucket = "ip"

var (
	db     *bolt.DB
	dbOpen bool
)

// A record is stored in the flat file data store
type Record struct {
	Hostname      string
	Provider      string
	StatusCode    int
	Stop          bool
	Wait          int
	Mesg          string
	CurrentIP     string
	LastIPUpdate  time.Time
	LastCheckDate time.Time
}

func dbInit() {
	var err error
	db, err = bolt.Open(databaseName, 0666, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatalf("Database Error: %s\n", err)
	}

	// Make sure the buckets exist in advance
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(hostBucket))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})

	dbOpen = true
}

// dbClose - closes the database connection
func dbClose() {
	dbOpen = false
	db.Close()
}

// writeIP - writes the ip to the database
func writeIP(id string, data string) error {
	if !dbOpen {
		dbInit()
	}

	err := db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(ipBucket))
		if err != nil {
			return err
		}

		d, err := json.Marshal(data)
		err = b.Put([]byte(id), d)
		if err != nil {
			return err
		}
		return nil
	})

	return err
}

// writeHost - writes the host record to the database
func writeHost(host Record) error {
	if !dbOpen {
		dbInit()
	}

	err := db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(hostBucket))

		if err != nil {
			return err
		}

		d, err := json.Marshal(host)
		if err != nil {
			return err
		}

		err = b.Put([]byte(host.Hostname), d)
		if err != nil {
			return err
		}
		return nil
	})

	return err
}

// readIP - reads the IP from the database
func readIP(id string) (string, error) {
	if !dbOpen {
		dbInit()
	}
	var lastIP string

	err := db.View(func(tx *bolt.Tx) error {
		var err error
		bucket := tx.Bucket([]byte(ipBucket))
		key := []byte(id)
		keyVal := bucket.Get(key)
		if keyVal != nil {
			lastIP, err = decodeIP(bucket.Get(key))
		} else {
			lastIP = ""
		}

		if err != nil {
			log.Fatal(err)
			return err
		}
		return nil
	})

	return lastIP, err
}

// readHost returns the record and error if any
func readHost(id string) (Record, error) {
	if !dbOpen {
		dbInit()
	}
	var host Record
	err := db.View(func(tx *bolt.Tx) error {
		var err error
		bucket := tx.Bucket([]byte(hostBucket))
		key := []byte(id)
		host, err = decodeHost(bucket.Get(key))

		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		return host, err
	}
	return host, err
}

// decodeIP - unmarshals json returning the IP
func decodeIP(data []byte) (string, error) {
	var ip string
	err := json.Unmarshal(data, &ip)
	if err != nil {
		return "", err
	}
	return ip, nil
}

// decodes unmarshals json returning a Record struct
func decodeHost(data []byte) (Record, error) {
	var host Record
	err := json.Unmarshal(data, &host)
	if err != nil {
		return host, err
	}
	return host, nil
}
