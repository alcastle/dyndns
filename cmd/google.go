package main

import (
	"strings"
	"time"
)

const googleDnsAPI = "domains.google.com/nic/update"

// responseEvalgoogle evaluates the HTTP responses from Googles DNS API
// Returns the response message from Google
func responseEvalGoogle(resp string, host Record) Record {
	// https://support.google.com/domains/answer/6147083?hl=en

	response := strings.TrimSpace(strings.ReplaceAll(resp, currentIP, ""))
	host.LastCheckDate = time.Now()

	switch response {
	case "good":
		host.CurrentIP = currentIP
		host.Mesg = "The update was successful."
		host.Wait = 0
		host.Stop = false
		host.LastIPUpdate = time.Now()
	case "nochg":
		host.CurrentIP = currentIP
		host.Mesg = "The supplied IP address is already set for this host."
		host.Wait = 0
		host.Stop = false
	case "nohost":
		host.Mesg = "The hostname does not exist, or does not have Dynamic DNS enabled."
		host.Wait = 0
		host.Stop = true
	case "badauth":
		host.Mesg = "The username / password combination is not valid for the specified host."
		host.Wait = 0
		host.Stop = true
	case "notfqdn":
		host.Mesg = "The supplied hostname is not a valid fully-qualified domain name."
		host.Wait = 0
		host.Stop = true
	case "badagent":
		host.Mesg = "Ensure the user agent is set in the request."
		host.Wait = 0
		host.Stop = true
	case "abuse":
		host.Mesg = "Dynamic DNS access for the hostname has been blocked."
		host.Wait = 0
		host.Stop = true
	case "911":
		host.Mesg = "An error happened on our end. Wait 5 minutes and retry."
		host.Wait = 300
		host.Stop = false
	default:
		if strings.Contains(response, "conflict") {
			host.Mesg = "A custom A or AAAA resource record conflicts with the update."
			host.Wait = 0
			host.Stop = true
		} else {
			host.Mesg = "Unrecognized response: " + response
			host.Wait = 0
			host.Stop = true
		}
	}

	return host
}

// UpdateDNSgoogle calls Google API to update the DNS record with the current IP Address.
func UpdateDNSgoogle(data hostMap, host Record) Record {

	var u = strings.Join(data.Domain["username"], "")
	var p = strings.Join(data.Domain["password"], "")
	var h = strings.Join(data.Domain["hostname"], "")

	endpoint := "https://" + u + ":" + p + "@" + googleDnsAPI +
		"?hostname=" + h + "&myip=" + strings.TrimSpace(currentIP)

	statusCode, resp := httpRequest(endpoint, "POST")
	host.StatusCode = statusCode
	host = responseEvalGoogle(resp, host)

	return host
}
