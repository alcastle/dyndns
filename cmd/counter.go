/*
User Counter

Will send an anonymous uuid consisting of operating system type (Linux, Windows, OSX), architecture, version of this
application, and a unique id for your application based on your mac address. This is only done once at start-up and
is only to count how many implementations and on what platforms.

Your current IP address will be visible in my webserver logs, but is not stored outside of logs or shared.
*/
package main

import (
	"encoding/base64"
	"log"
	"os"
	"strconv"
)

const counterUrl = "http://updater-ddns.castle.industries"

// uniqUserCounter
func uniqUserCounter() {
	if analytics == true && siteReachable(counterUrl) {
		// Url encode OS + Architecture
		clientInfo := base64.URLEncoding.EncodeToString([]byte(clientInfo()))
		// Generate anonymous uuid
		uuid := appName + appVersion + "-" + clientInfo + "-" + strconv.FormatUint(getMacAddressUuid(), 10)
		filename := "." + appName + "_ID"
		// Build URL to call
		url := counterUrl + "?u=" + uuid + "&c=" + clientInfo

		if _, err := os.Stat(filename); os.IsNotExist(err) {
			log.Printf("File %s doesn't exist, creating it\n", filename)
			createFile(filename)
			writeToFile(filename, uuid)
			go httpRequest(url)
		} else if filesize(filename) > 0 {
			c := readFile(filename)
			if c != uuid {
				writeToFile(filename, uuid)
			}
			go httpRequest(url)
		} else if filesize(filename) == 0 {
			writeToFile(filename, uuid)
			go httpRequest(url)
		} else {
			// File exists already - Assume data has been counted previously
		}
	}
}
