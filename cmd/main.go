package main

import (
	"github.com/jasonlvhit/gocron"
	"github.com/spf13/viper"
	"log"
	"strings"
	"time"
)

var (
	updateInterval uint64
	config         configuration
	clientOS       string
	clientArch     string
	userAgent      string
	analytics      bool
	currentIP      string
)

type configuration struct {
	Domains []hostMap `mapstructure:"hostList"`
}

type hostMap struct {
	Domain map[interface{}][]string `mapstructure:"domain"`
}

// updateDNS calls the appropriate provider API to update the DNS record with the current IP Address.
func updateDNS(host Record, data hostMap) Record {

	if host.Stop == true {
		log.Printf("Host won't update due to last reason: %s\n", host.Mesg)
		return host
	} else if host.Wait > 0 {
		// Check if we need to wait until or future loops
		// no-ip wants a (1800 seconds) 30 min delay in some situations
		// compare lastUpdate, interval and now
		t1 := time.Now()
		t2 := t1.Add(time.Second * time.Duration(host.Wait))
		t3 := t2.Sub(host.LastCheckDate)

		var since int = int(t3.Seconds())
		waitMin := host.Wait / 60
		if since > host.Wait {
			waitMin = int(since-host.Wait) / 60
		}

		if since >= host.Wait {
			log.Printf("%s DNS provider has requested we wait %v more minutes until we try an update",
				host.Provider, waitMin)
		}
	} else if host.CurrentIP == currentIP {
		log.Printf("[%s] %s already has correct IP Address\n", host.Provider, host.Hostname)
	} else {
		log.Printf("Looking for %s DNS provider for %s\n", data.Domain["provider"], data.Domain["hostname"])
		switch host.Provider {
		case "google":
			host = UpdateDNSgoogle(data, host)
		case "easydns":
			host = UpdateDNSeasydns(data, host)
		case "no-ip":
			host = UpdateDNSnoip(data, host)
		case "dynu":
			host = UpdateDNSdynu(data, host)
		case "custom":
			host = UpdateDNScustom(data, host)
		default:
			// do nothing
			log.Fatal("Unsupported provider. Please double check your config.yaml file.")
		}
	}

	log.Printf("Provider [%s] StatusCode:%d Mesg:%s \n ", host.Provider, host.StatusCode, host.Mesg)
	// Update a flat file database with the information returned
	if host.Provider != "" {
		err := writeHost(host)

		if err != nil {
			log.Fatal(err)
		}
	} else {
		//log.Printf("Host provider is not set for %s", strings.Join(data.Domain["hostname"], ""))
		log.Printf("Host provider is not set for %s", data.Domain["hostname"])
	}
	return host
}

// run loops over the list of domains from the config.yaml file and updates DNS records as necessary.
func run() bool {
	currentIP = getCurrentIP()
	if currentIP == "" {
		log.Println("Unable to determine your IP Address. Please check your network connection.")

		return false
	}

	writeIP("currentIP", currentIP)
	_, err := readIP("currentIP")
	if err != nil {
		log.Fatal(err)
	}

	// Loop thru list of hosts to update from config file
	for _, data := range config.Domains {
		host, _ := readHost(strings.Join(data.Domain["hostname"], ""))
		found := host.Hostname
		if host.Hostname == "" {
			found = "No record found"
		}
		log.Printf("Looking for %s. Found: %s\n", strings.Join(data.Domain["hostname"], ""), found)

		// Empty host record
		if host.Provider == "" {
			host.Hostname = strings.Join(data.Domain["hostname"], "")
			host.Provider = strings.Join(data.Domain["provider"], "")
		}

		if host.CurrentIP != currentIP {
			host = updateDNS(host, data)
		} else {
			log.Printf("Nothing to update for %s %s.\n\n", data.Domain["provider"], data.Domain["hostname"])
		}

		log.Println("-----------------\n-----------------")
	}

	log.Println("-----------------\n-----------------")

	return true
}

// init gets processed prior to main. Reads in our config.yaml file setting variables.
func init() {
	// Show line numbers of errors
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Name of the configuration file
	viper.SetConfigName("config")
	viper.AddConfigPath(".")

	// Find and read the configuration file
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("Fatal error reading config file: %s \n", err)
	}

	// Due to nested yaml structure need to unmarshal to use the configuration data
	err = viper.Unmarshal(&config)
	if err != nil {
		log.Fatalln(err)
	}

	// Default Interval is 5 minutes (300/60)
	viper.SetDefault("updateInterval", 300)
	viper.SetDefault("analytics", true)

	// This feature isn't implemented
	//viper.SetDefault("offline", "")

	updateInterval = viper.GetUint64("updateInterval")
	analytics = viper.GetBool("analytics")
	userAgent = setUserAgent()
	uniqUserCounter()
	dbInit()
}

// Based on the updateInterval setting in config.yaml will run the primary run() method every X seconds.
func executeCronJob() {
	gocron.Every(updateInterval).Seconds().Do(run)
	<-gocron.Start()
}

func main() {
	defer dbClose()

	run()
	log.Printf("Will run again %d seconds.\n", updateInterval)
	executeCronJob()
}
